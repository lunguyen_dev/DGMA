## App Demo App

<img src="screenshots/1.png" width="200" /> <img src="screenshots/2.png" width="200" /> <img src="screenshots/3.png" width="200" /><img src="screenshots/4.png" width="200" />

## Get Started

### 1. System Requirements
* Globally installed [node](https://nodejs.org/en/)
* Globally installed [react-native CLI](https://facebook.github.io/react-native/docs/getting-started.html)

### 2. Installation
On the command prompt run the following commands

```sh
$ git clone git@gitlab.com:lunguyen_dev/DGMA.git
$ cd DGMA/
$ npm install or yarn install
```
```sh
$ react-native run-android (for Android)
$ react-native run-ios (for iOS)
```


### 3. Simulate for iOS
**Method One**
* Open the project in XCode from **ios/.xcodeproj**
* Hit the play button.

**Method Two**
* Run the following command in your terminal
    ```sh
    $ react-native run-ios
    ```

### 4. Simulate for Android
* Make sure you have an **Android emulator** installed and running.
* Run the following command in your terminal
    ```sh
    $ react-native run-android
    ```

